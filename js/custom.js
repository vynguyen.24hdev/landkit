window.onload = function () {
  //slider
  const sliderItems = document.querySelectorAll(".card-img-start");
  const sliderCaption = document.querySelectorAll(".slider-caption");
  const prevButton = document.querySelector(".previous");
  const nextButton = document.querySelector(".next");
  let currentIndex = 0;
  

  function showItem(index, direction) {
    sliderItems[currentIndex].classList.remove("active");
    sliderCaption[currentIndex].classList.remove("active", direction);

    let newIndex = (index + sliderItems.length) % sliderItems.length;
    sliderItems[newIndex].classList.add("active");
    sliderCaption[newIndex].classList.add("active", direction);
    currentIndex = newIndex;
  }

  function prevItem() {
    showItem(currentIndex - 1, "slider-right");
  }

  function nextItem() {
    showItem(currentIndex + 1, "slider-left");
  }

  prevButton.addEventListener("click", prevItem);
  nextButton.addEventListener("click", nextItem);

  showItem(currentIndex);

  

  // open/close menu
  let barsBtn = document.getElementById("bar-btn");
  let showBtn = document.getElementById("navbarCollapse");
  let closeBtn = document.getElementById("close-btn");

  barsBtn.onclick = function () {
    showBtn.classList.add("show");
    showBtn.classList.remove("collapsing");
  };
  closeBtn.onclick = function () {
    showBtn.classList.remove("show");
    showBtn.classList.add("collapsing");
  };
  window.addEventListener("resize", function () {
    if (window.innerWidth >= 992) {
      showBtn.classList.remove("show");
    }
  });
  let dropendBtn = document.querySelectorAll(".dropend");
  let dropdownMenuBtn = document.querySelectorAll(".dropend .dropdown-menu");
  let dropdownIconBtn = document.querySelectorAll(".toggle-icon");

  for (let i = 0; i < dropendBtn.length; i++) {
    dropendBtn[i].addEventListener("click", function () {
      for (let j = 0; j < dropdownMenuBtn.length; j++) {
        if (j !== i) {
          dropdownMenuBtn[j].style.display = "none";
          if (dropdownIconBtn[j]) {
            dropdownIconBtn[j].style.transform = "rotate(0deg)";
          }
        }
      }
      if (dropdownMenuBtn[i].style.display === "none") {
        dropdownMenuBtn[i].style.display = "block";
        if (dropdownIconBtn[i]) {
          dropdownIconBtn[i].style.transform = "rotate(180deg)";
        }
      } else {
        dropdownMenuBtn[i].style.display = "none";
        if (dropdownIconBtn[i]) {
          dropdownIconBtn[i].style.transform = "rotate(0deg)";
        }
      }
    });
  }
  // validate input

  const form = document.querySelector("form");
  const nameInput = document.querySelector("#cardName");
  const emailInput = document.querySelector("#cardEmail");
  const passwordInput = document.querySelector("#cardPassword");
  const nameError = document.querySelector("#nameError");
  const emailError = document.querySelector("#emailError");
  const passwordError = document.querySelector("#passwordError");

  function validateName() {
    const value = nameInput.value.trim();
    if (value === "") {
      nameError.textContent = "Name is required";
      return false;
    } else {
      nameError.textContent = "";
      return true;
    }
  }

  function validateEmail() {
    const value = emailInput.value.trim();
    if (value === "") {
      emailError.textContent = "Email is required";
      return false;
    } else if (!/\S+@\S+\.\S+/.test(value)) {
      emailError.textContent = "Email is invalid";
      return false;
    } else {
      emailError.textContent = "";
      return true;
    }
  }

  function validatePassword() {
    const value = passwordInput.value.trim();
    if (value.length < 8) {
      passwordError.textContent = "Password must be at least 8 characters";
      return false;
    } else {
      passwordError.textContent = "";
      return true;
    }
  }

  function validateForm() {
    const isValidName = validateName();
    const isValidEmail = validateEmail();
    const isValidPassword = validatePassword();
    return isValidName && isValidEmail && isValidPassword;
  }

  form.addEventListener("submit", (event) => {
    event.preventDefault();
    if (validateForm()) {
      console.log("Name:", nameInput.value);
      console.log("Email:", emailInput.value);
      console.log("Password:", passwordInput.value);
    }
  });
  // typing
  const textEl = document.getElementById("multiple-text");
  const words = ["Developers.", "Founders.", "Designers."];
  let wordIndex = 0;
  let letterIndex = 0;

  function type() {
    if (letterIndex < words[wordIndex].length) {
      textEl.textContent += words[wordIndex].charAt(letterIndex);
      letterIndex++;
      setTimeout(type, 100);
    } else {
      setTimeout(erase, 1000);
    }
  }

  function erase() {
    if (letterIndex > 0) {
      textEl.textContent = words[wordIndex].substring(0, letterIndex - 1);
      letterIndex--;
      setTimeout(erase, 50);
    } else {
      wordIndex++;
      if (wordIndex >= words.length) {
        wordIndex = 0;
      }
      setTimeout(type, 500);
    }
  }

  type();

  // annual, monthly
  const target = document.querySelector(".price");
  const checkbox = document.querySelector("#billingSwitch");
  let currentValue = Number(target.textContent);
  let finalValue = checkbox.checked ? 49 : 29;
  let step = currentValue < finalValue ? 1 : -1;

  function updateValue() {
    currentValue += step;
    target.textContent = currentValue;
    if (currentValue !== finalValue) {
      setTimeout(updateValue, 20);
    }
  }

  checkbox.addEventListener("change", function () {
    finalValue = this.checked ? 49 : 29;
    step = currentValue < finalValue ? 1 : -1;
    setTimeout(updateValue, 20);
  });

  function countUp(targetElement, startVal, endVal) {
    let currentVal = startVal;
    const increment = endVal > startVal ? 1 : -1;
    const duration = 1000;
    const stepTime = Math.abs(Math.floor(duration / (endVal - startVal)));

    const timer = setInterval(() => {
      currentVal += increment;
      targetElement.innerText = currentVal;
      if (currentVal === endVal) {
        clearInterval(timer);
      }
    }, stepTime);
  }

  // animation scroll and page reload
  const span1 = document.querySelectorAll(".countup-1");
  for (let i = 0; i < span1.length; i++) {
    countUp(span1[i], 0, 100);
  }
  const span2 = document.querySelector(".countup-2");
  countUp(span2, 0, 24);
  const span3 = document.querySelector(".countup-3");
  countUp(span3, 0, 7);
  var elements = document.querySelectorAll(
    ".slide-top, .slide-left, .slide-right"
  );

  window.addEventListener("scroll", function () {
    for (var i = 0; i < elements.length; i++) {
      var bounding = elements[i].getBoundingClientRect();
      if (
        bounding.top >= 0 &&
        bounding.bottom <=
          (window.innerHeight || document.documentElement.clientHeight)
      ) {
        elements[i].classList.add("active");
      }
    }
  });

  
 


};



